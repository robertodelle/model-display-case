#include <IRremote.h>
#define MAX_TIME 150

int RECV_PIN = 5;
IRrecv irrecv(RECV_PIN);
decode_results results;
long lastPressTime = 0;
int state = LOW;
int oneREG = 0;


void setup()
{ 
  digitalWrite(12, 0);  // first state
  pinMode(12,OUTPUT);
  irrecv.enableIRIn(); // Start the receiver
}

void loop() {   
  if (irrecv.decode(&results)) {   
   // one ------------------------------------------------
     if (results.value == 0xFFA25D) {
      if (millis() - lastPressTime > MAX_TIME) {
        state = 1 - state;
        oneREG = 1;
      }
      lastPressTime = millis();
    }
    irrecv.resume(); // Receive the next value
    }
//---------------------------------------------------------       
// one
        if (oneREG == 1) {  
      digitalWrite(12,1); 
      delay(1000);
     digitalWrite(12,0);
        
        oneREG = 0;
    }
   }
