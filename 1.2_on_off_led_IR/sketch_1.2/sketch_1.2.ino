#include <IRremote.h>
#define MAX_TIME 150

int RECV_PIN = 5;
IRrecv irrecv(RECV_PIN);
decode_results results;
long lastPressTime = 0;
int state = LOW;
int speed1 = 150;

int arrowupREG = 0;
int oneREG = 0;
int twoREG = 0;
int threeREG = 0;
int fourREG = 0;
int arrowdownREG = 0;
int arrowsxREG = 0;
int arrowdxREG = 0;

// engines powered by 7 volts step up down converter
int speedPinL = 7; // PIN PWM engine 1
int speedPinR = 8; // PIN PWM engine 2

void setup()
{ 
  digitalWrite(13, 0);  // first state
  pinMode(13,OUTPUT);
  digitalWrite(3, 0);  // first state
  pinMode(3,OUTPUT);
  digitalWrite(4, 0);  // first state
  pinMode(4,OUTPUT);
  digitalWrite(5, 0);  // first state
  pinMode(5,OUTPUT);
  pinMode(speedPinL, OUTPUT);
  pinMode(speedPinR, OUTPUT);
  irrecv.enableIRIn(); // Start the receiver
}

void loop() {   
  if (irrecv.decode(&results)) {   
    // arrow up -----------------------------------------
    if (results.value == 0x9EB92) {
      if (millis() - lastPressTime > MAX_TIME) {
        arrowupREG = 1;
      }
      lastPressTime = millis();
    }
      irrecv.resume(); // Receive the next value
   // one ------------------------------------------------
     if (results.value == 0xFFA25D) {
      if (millis() - lastPressTime > MAX_TIME) {
        state = 1 - state;
        oneREG = 1;
      }
      lastPressTime = millis();
    }
    irrecv.resume(); // Receive the next value
    
   // two ------------------------------------------------
     if (results.value == 0x80B92) {
      if (millis() - lastPressTime > MAX_TIME) {
        state = 1 - state;
        twoREG = 1;
      }
      lastPressTime = millis();
    }
    irrecv.resume(); // Receive the next value
    
    // three --------------------------------------------
     if (results.value == 0x40B92) {
      if (millis() - lastPressTime > MAX_TIME) {
        state = 1 - state;
        threeREG = 1;
      }
      lastPressTime = millis();
    }
    irrecv.resume(); // Receive the next value
    
     // four --------------------------------------------
     if (results.value == 0xC0B92) {
      if (millis() - lastPressTime > MAX_TIME) {
        state = 1 - state;
        fourREG = 1;
      }
      lastPressTime = millis();
    }
    irrecv.resume(); // Receive the next value
    
    // arrow down -----------------------------------------
    if (results.value == 0x5EB92) {
      if (millis() - lastPressTime > MAX_TIME) {
        arrowdownREG = 1;
      }
      lastPressTime = millis();
    }
     irrecv.resume(); // Receive the next value
     
    // arrow sx -----------------------------------------
    if (results.value == 0xDEB92) {
      if (millis() - lastPressTime > MAX_TIME) {
        arrowsxREG = 1;
      }
      lastPressTime = millis();
    }
      irrecv.resume(); // Receive the next value

      // arrow dx -----------------------------------------
    if (results.value == 0x3EB92) {
      if (millis() - lastPressTime > MAX_TIME) {
        arrowdxREG = 1;
      }
      lastPressTime = millis();
    }
      irrecv.resume(); // Receive the next value

    }
//---------------------------------------------------------       
// arrow up
        if (oneREG == 1) {  
      digitalWrite(13,1); 
      delay(1000);
     digitalWrite(13,0);
        arrowupREG = 0;
        oneREG = 0;
    }
   
     if (arrowupREG == 1) {
        if (twoREG == 1) {  
       // go forward two step
       analogWrite(speedPinL, 185);
      analogWrite(speedPinR, 200);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,1);
      digitalWrite(5,0);  
      delay(2000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowupREG = 0;
        twoREG = 0;
    }
    }
    if (arrowupREG == 1) {
        if (threeREG == 1) {  
       // go forward three step
       analogWrite(speedPinL, 185);
      analogWrite(speedPinR, 200);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,1);
      digitalWrite(5,0);  
      delay(3000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowupREG = 0;
        threeREG = 0;
    }
    }
    if (arrowupREG == 1) {
        if (fourREG == 1) {  
       // go forward four step
       analogWrite(speedPinL, 185);
      analogWrite(speedPinR, 200);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,1);
      digitalWrite(5,0);  
      delay(4000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowupREG = 0;
        fourREG = 0;
    }
    }
// arrow down -------------------------------------------
if (arrowdownREG == 1) {
        if (oneREG == 1) {  
       // go back one step
       analogWrite(speedPinL, 185);
      analogWrite(speedPinR, 200);
      digitalWrite(2,0); 
      digitalWrite(3,1);
      digitalWrite(4,0);
      digitalWrite(5,1);  
      delay(1000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowdownREG = 0;
        oneREG = 0;
    }
    }
     if (arrowdownREG == 1) {
        if (twoREG == 1) {  
       // go back two step
       analogWrite(speedPinL, 185);
      analogWrite(speedPinR, 200);
      digitalWrite(2,0); 
      digitalWrite(3,1);
      digitalWrite(4,0);
      digitalWrite(5,1);  
      delay(2000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowdownREG = 0;
        twoREG = 0;
    }
    }
    if (arrowdownREG == 1) {
        if (threeREG == 1) {  
       // go back three step
       analogWrite(speedPinL, 185);
      analogWrite(speedPinR, 200);
      digitalWrite(2,0); 
      digitalWrite(3,1);
      digitalWrite(4,0);
      digitalWrite(5,1);  
      delay(3000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowdownREG = 0;
        threeREG = 0;
    }
    }
    if (arrowdownREG == 1) {
        if (fourREG == 1) {  
       // go back four step
       analogWrite(speedPinL, 185);
      analogWrite(speedPinR, 200);
      digitalWrite(2,0); 
      digitalWrite(3,1);
      digitalWrite(4,0);
      digitalWrite(5,1);  
      delay(4000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowdownREG = 0;
        fourREG = 0;
    }
    }
// arrow sx -------------------------------------------
if (arrowsxREG == 1) {
        if (oneREG == 1) {  
       // go sx one step
       analogWrite(speedPinL, 150);
      analogWrite(speedPinR, 150);
      digitalWrite(2,0); 
      digitalWrite(3,1);
      digitalWrite(4,1);
      digitalWrite(5,0);  
      delay(500);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowsxREG = 0;
        oneREG = 0;
    }
    }
    if (arrowsxREG == 1) {
        if (twoREG == 1) {  
       // go sx two step
       analogWrite(speedPinL, 150);
      analogWrite(speedPinR, 150);
      digitalWrite(2,0); 
      digitalWrite(3,1);
      digitalWrite(4,1);
      digitalWrite(5,0);  
      delay(1000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowsxREG = 0;
        twoREG = 0;
    }
    }
    if (arrowsxREG == 1) {
        if (threeREG == 1) {  
       // go sx three step
       analogWrite(speedPinL, 150);
      analogWrite(speedPinR, 150);
      digitalWrite(2,0); 
      digitalWrite(3,1);
      digitalWrite(4,1);
      digitalWrite(5,0);  
      delay(1500);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowsxREG = 0;
        threeREG = 0;
    }
    }
    if (arrowsxREG == 1) {
        if (fourREG == 1) {  
       // go sx four step
       analogWrite(speedPinL, 150);
      analogWrite(speedPinR, 150);
      digitalWrite(2,0); 
      digitalWrite(3,1);
      digitalWrite(4,1);
      digitalWrite(5,0);  
      delay(2000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowsxREG = 0;
        fourREG = 0;
    }
    }
// arrow dx -------------------------------------------
if (arrowdxREG == 1) {
        if (oneREG == 1) {  
       // go dx one step
       analogWrite(speedPinL, 150);
      analogWrite(speedPinR, 150);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,1);  
      delay(500);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowdxREG = 0;
        oneREG = 0;
    }
    }
if (arrowdxREG == 1) {
        if (twoREG == 1) {  
       // go dx two step
       analogWrite(speedPinL, 150);
      analogWrite(speedPinR, 150);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,1);  
      delay(1000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowdxREG = 0;
        twoREG = 0;
    }
    }
    if (arrowdxREG == 1) {
        if (threeREG == 1) {  
       // go dx three step
       analogWrite(speedPinL, 150);
      analogWrite(speedPinR, 150);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,1);  
      delay(1500);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowdxREG = 0;
        threeREG = 0;
    }
    }
if (arrowdxREG == 1) {
        if (fourREG == 1) {  
       // go dx four step
       analogWrite(speedPinL, 150);
      analogWrite(speedPinR, 150);
      digitalWrite(2,1); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,1);  
      delay(2000);
      analogWrite(speedPinL, 0);
      analogWrite(speedPinR, 0);
      digitalWrite(2,0); 
      digitalWrite(3,0);
      digitalWrite(4,0);
      digitalWrite(5,0);
        arrowdxREG = 0;
        fourREG = 0;
    }
    }
}
